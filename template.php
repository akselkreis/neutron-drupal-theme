<?php

/**
 * Add body classes if certain regions have content.
 */

function neutron_menu_link__main_menu(array $variables) {
		$element = $variables['element'];
		$sub_menu = '';

		// var_dump($element['#original_link']);

		if($element['#original_link']['depth'] == 1){
			$type = 'main';
		}else{
			$type = 'vertical';
		}

		unset($element['#attributes']['class']);

		$controller = "";

		if($element['#original_link']['has_children'] == 1){
			$element['#attributes']['class'][] = "hasChildren";
			$controller = '<a href="#" class="icon icon-sub-menu-toggle"></a>';
		}

		$element['#attributes']['class'][] = "item";
		$element['#localized_options']['attributes']['class'][] = "item";

		if ($element['#below']) {
			$sub_menu = drupal_render($element['#below']);
		}

		$output = l($element['#title'], $element['#href'], $element['#localized_options'] );
		// return '<ul class="navigation ' . $type .'"><li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li></ul>\n";
		return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $controller . $sub_menu . "</li>";
}


//Don’t wrap the navigation, as that gets covered in the above function
function neutron_menu_tree_main_menu($variables) {
	return '<ul class="navigation main">' . $variables['tree'] . '</ul>';
	// return '<ul class="navigation vertical">' . $variables['tree'] . '</ul>';
	// return $variables['tree'];
}



function neutron_preprocess_html(&$variables) {
	// Add stylesheets & html5shiv for IE

	if(preg_match('/(?i)msie [8]/',$_SERVER['HTTP_USER_AGENT'])){
	 drupal_add_css(path_to_theme() . '/css/build/stripped/ie8.css');
	 drupal_add_js(path_to_theme() . '/js/libs/html5shiv.min.js');
	}
}


function neutron_form_alter(&$form, &$form_state, $form_id) {
	if ($form_id == 'search_block_form') {
		// HTML5 placeholder attribute
		$form['search_block_form']['#attributes']['placeholder'] = t('Search');
	}
}

function neutron_css_alter(&$css) {
	unset($css[drupal_get_path('module', 'system') . '/defaults.css']);
	unset($css[drupal_get_path('module', 'system') . '/system.css']);
	unset($css[drupal_get_path('module', 'system') . '/system.theme.css']);
	unset($css[drupal_get_path('module', 'user') . '/user.css']);
	unset($css[drupal_get_path('module', 'ckeditor') . '/ckeditor.css']);
	unset($css[drupal_get_path('module', 'ctools') . '/css/ctools.css']);
	unset($css[drupal_get_path('module', 'field') . '/theme/field.css']);
	unset($css[drupal_get_path('module', 'node') . '/node.css']);
	unset($css[drupal_get_path('module', 'search') . '/search.css']);
	unset($css[drupal_get_path('module', 'views') . '/css/views.css']);
	unset($css[drupal_get_path('module', 'webform') . '/css/webform.css']);

	if(preg_match('/(?i)msie [8]/',$_SERVER['HTTP_USER_AGENT'])){
		unset($css[drupal_get_path('theme', 'neutron') . '/css/build/minified/styles.css']);
	}
}

/**
 * Override or insert variables into the page template for HTML output.
 */
function neutron_process_html(&$variables) {
	// Hook into color.module.
	if (module_exists('color')) {
		_color_html_alter($variables);
	}
}

/**
 * Override or insert variables into the page template.
 */
function neutron_process_page(&$variables) {
	// Hook into color.module.
	if (module_exists('color')) {
		_color_page_alter($variables);
	}
	// Always print the site name and slogan, but if they are toggled off, we'll
	// just hide them visually.
	$variables['hide_site_name']   = theme_get_setting('toggle_name') ? FALSE : TRUE;
	$variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
	if ($variables['hide_site_name']) {
		// If toggle_name is FALSE, the site_name will be empty, so we rebuild it.
		$variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
	}
	if ($variables['hide_site_slogan']) {
		// If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.
		$variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
	}
	// Since the title and the shortcut link are both block level elements,
	// positioning them next to each other is much simpler with a wrapper div.
	if (!empty($variables['title_suffix']['add_or_remove_shortcut']) && $variables['title']) {
		// Add a wrapper div using the title_prefix and title_suffix render elements.
		$variables['title_prefix']['shortcut_wrapper'] = array(
			'#markup' => '<div class="shortcut-wrapper clearfix">',
			'#weight' => 100,
		);
		$variables['title_suffix']['shortcut_wrapper'] = array(
			'#markup' => '</div>',
			'#weight' => -99,
		);
		// Make sure the shortcut link is the first item in title_suffix.
		$variables['title_suffix']['add_or_remove_shortcut']['#weight'] = -100;
	}
}

/**
 * Implements hook_preprocess_maintenance_page().
 */
function neutron_preprocess_maintenance_page(&$variables) {
	// By default, site_name is set to Drupal if no db connection is available
	// or during site installation. Setting site_name to an empty string makes
	// the site and update pages look cleaner.
	// @see template_preprocess_maintenance_page
	if (!$variables['db_is_active']) {
		$variables['site_name'] = '';
	}
	drupal_add_css(drupal_get_path('theme', 'neutronbootstrap') . '/css/maintenance-page.css');
}

/**
 * Override or insert variables into the maintenance page template.
 */
function neutron_process_maintenance_page(&$variables) {
	// Always print the site name and slogan, but if they are toggled off, we'll
	// just hide them visually.
	$variables['hide_site_name']   = theme_get_setting('toggle_name') ? FALSE : TRUE;
	$variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
	if ($variables['hide_site_name']) {
		// If toggle_name is FALSE, the site_name will be empty, so we rebuild it.
		$variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
	}
	if ($variables['hide_site_slogan']) {
		// If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.
		$variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
	}
}

/**
 * Override or insert variables into the node template.
 */
function neutron_preprocess_node(&$variables) {
	if ($variables['view_mode'] == 'full' && node_is_page($variables['node'])) {
		$variables['classes_array'][] = 'node-full';
	}
}

/**
 * Override or insert variables into the block template.
 */
function neutron_preprocess_block(&$variables) {
	// In the header region visually hide block titles.
	if ($variables['block']->region == 'header') {
		$variables['title_attributes_array']['class'][] = 'element-invisible';
	}
}

/**
 * Implements theme_menu_tree().
 */
function neutron_menu_tree($variables) {
	return '<ul class="menu clearfix">' . $variables['tree'] . '</ul>';
}

/**
 * Implements theme_field__field_type().
 */
function neutron_taxonomy_term_reference($variables) {
	$output = '';

	// Render the label, if it's not hidden.
	if (!$variables['label_hidden']) {
		$output .= '<h3 class="field-label">' . $variables['label'] . ': </h3>';
	}

	// Render the items.
	$output .= ($variables['element']['#label_display'] == 'inline') ? '<ul class="links inline">' : '<ul class="links">';
	foreach ($variables['items'] as $delta => $item) {
		$output .= '<li class="taxonomy-term-reference-' . $delta . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</li>';
	}
	$output .= '</ul>';

	// Render the top-level DIV.
	$output = '<div class="' . $variables['classes'] . (!in_array('clearfix', $variables['classes_array']) ? ' clearfix' : '') . '"' . $variables['attributes'] .'>' . $output . '</div>';

	return $output;
}
