<?php
    
function neutron_form_system_theme_settings_alter(&$form, &$form_state) {
    $form['theme_settings']['chrome_color'] = array(
        '#type' => 'textfield',
        '#title' => t('Mobile Chrome Theme Color'),
        '#default_value' => theme_get_setting('chrome_color'),
    );
}